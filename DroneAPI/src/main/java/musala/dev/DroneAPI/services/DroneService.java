package musala.dev.DroneAPI.services;

import musala.dev.DroneAPI.enums.DroneModel;
import musala.dev.DroneAPI.enums.DroneState;
import musala.dev.DroneAPI.model.Drone;
import musala.dev.DroneAPI.model.Medication;
import musala.dev.DroneAPI.repository.DroneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@Service
public class DroneService implements IDroneService {
    @Autowired
    private DroneRepository droneRepository;

    public String saveDrone(Drone drone){
        Drone gottenDrone = droneRepository.findBySerialNumber(drone.getSerialNumber());
        if(gottenDrone!=null){
            return "Drone with serial number already exist";
        }
        droneRepository.save(drone);
        return "Drone saved successfully";
    }

    public Iterable<Drone> getAllDrones(){
        return droneRepository.findAll();
    }

    public Iterable<Drone> getByModel(DroneModel droneModel) {
        return droneRepository.findByDroneModel(droneModel);
    }
    public String AddMedication(Medication medication, String sn){
        Drone gottenDrone = droneRepository.findBySerialNumber(sn);
        if(gottenDrone != null){
            if(gottenDrone.getBatteryCapacity()<25){
                return "Battery is below 25%. Unable to load medication";
            }
            int maxWeight = gottenDrone.getWeightLimit();
            List<Medication> meds = gottenDrone.getMedications();
            int weight=0;
            for(Medication med : meds){
                weight += med.getWeight();
            }
            if(weight + medication.getWeight() > maxWeight){
                return  "Maximum weight exceeded for Drone";
            }
            meds.add(medication);
            gottenDrone.setMedications(meds);
            gottenDrone.setDroneState(DroneState.valueOf("LOADING"));
            droneRepository.save(gottenDrone);
            return  "Medication added successfully to Drone";
        }
        return  "Invalid drone serial number";
    }

    public  List<Medication> getDroneItems(String serialNumber){
        Drone gottenDrone = droneRepository.findBySerialNumber(serialNumber);
        if(gottenDrone != null){
            return gottenDrone.getMedications();
        }
       return  new ArrayList<>();
    }

    public List<Drone> getAvailableDrones(){
        return droneRepository.findByDroneState(DroneState.valueOf("IDLE"));
    }

    public String getBatteryLevel(String serialNumber){
        Drone gottenDrone = droneRepository.findBySerialNumber(serialNumber);
        if(gottenDrone!=null){
            return String.valueOf(gottenDrone.getBatteryCapacity()) + "%";
        }else {
            return "Invalid drone serial number";
        }
    }

    private boolean isValidState(String state){
        DroneState st;
        try {
            st = DroneState.valueOf(state);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }
    public String setDroneState(String serialNumber, String state) {
        Drone drone = droneRepository.findBySerialNumber(serialNumber);
        if(drone==null) {
            return "Invalid drone serial number";
        }
        if(!isValidState(state)){
            return "Invalid drone state entered";
        }
        drone.setDroneState(DroneState.valueOf(state));
        droneRepository.save(drone);
        return "Drone ("+serialNumber+") successfully set to "+state;
    }
    private boolean logFileExists(){
        try {
            File file = new File("Battery log.txt");
            if(file.exists()){
                return true;
            }
            file.createNewFile();
            return true;
        } catch (IOException e) {
            System.out.println("An error occurred.");
            return  false;
        }
    }
    public void batteryLevelMonitor (){
       if(logFileExists()){
           int period = 5000; //Runs every five seconds

           Timer timer = new Timer();
           timer.scheduleAtFixedRate(new TimerTask() {
               public void run() {
                   setBatteryLevel();
               }
           }, 0, period);
       }
    }
    private void writeToFile(String text){
        try {
            FileWriter myWriter = new FileWriter("Battery log.txt",true);
            myWriter.write(text);
            System.out.println(text+" written");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred while writing to file.");
        }
    }

    //Periodically reduce battery level for active drones
    public void setBatteryLevel(){
        //DELIVERING, DELIVERED and RETURNING drones are considered active drones
        List<Drone> deliveringDrones = droneRepository.findByDroneState(DroneState.valueOf("DELIVERING"));
        List<Drone> deliveredDrones = droneRepository.findByDroneState(DroneState.valueOf("DELIVERED"));
        List<Drone> returningDrones = droneRepository.findByDroneState(DroneState.valueOf("RETURNING"));
        deliveredDrones.addAll(deliveringDrones);
        returningDrones.addAll(deliveredDrones);
        //log: number of active drones
        for(Drone drone : returningDrones) {
            int currentCapacity = drone.getBatteryCapacity();
            if(currentCapacity>0){
                drone.setBatteryCapacity(currentCapacity  - 1);
                writeToFile("Drone ("+123+") battery level now is ("+(currentCapacity  - 1)+")");
                droneRepository.save(drone);
            }else {
                writeToFile("Drone ("+drone.getSerialNumber()+") battery level now is ("+currentCapacity+")");
            }
            writeToFile(System.getProperty("line.separator" ));
        }
    }
}
