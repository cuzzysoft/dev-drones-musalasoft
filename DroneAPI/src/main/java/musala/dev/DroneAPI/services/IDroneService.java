package musala.dev.DroneAPI.services;

import musala.dev.DroneAPI.enums.DroneModel;
import musala.dev.DroneAPI.model.Drone;
import musala.dev.DroneAPI.model.Medication;

import java.util.List;

public interface IDroneService {
    String saveDrone(Drone drone);
    Iterable<Drone> getAllDrones();
    Iterable<Drone> getByModel(DroneModel droneModel);
    String AddMedication(Medication medication, String sn);
    List<Medication> getDroneItems(String serialNumber);
    List<Drone> getAvailableDrones();
    String getBatteryLevel(String serialNumber);
    String setDroneState(String serialNumber, String state);
    void batteryLevelMonitor();
}

