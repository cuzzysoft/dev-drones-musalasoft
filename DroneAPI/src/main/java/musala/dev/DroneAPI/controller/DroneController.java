package musala.dev.DroneAPI.controller;

import musala.dev.DroneAPI.dao.DroneStateDao;
import musala.dev.DroneAPI.enums.DroneModel;
import musala.dev.DroneAPI.model.Drone;
import musala.dev.DroneAPI.model.Medication;
import musala.dev.DroneAPI.services.IDroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DroneController {
    @Autowired
    private final IDroneService _droneService;

    public DroneController(IDroneService droneService){
        _droneService = droneService;
        _droneService.batteryLevelMonitor();
    }

    //Create New Drone
    @PostMapping("/saveDrone")
    public String saveDrone(@RequestBody Drone drone){
        return _droneService.saveDrone(drone);
    }

    //Get All Drones
    @GetMapping("/getDrones")
    public Iterable<Drone> getAll(){
        return _droneService.getAllDrones();
    }

    //Get Drone by Model
    @GetMapping("/getDrones/{droneModel}")
    public Iterable<Drone> getByModel(DroneModel droneModel){
        return _droneService.getByModel(droneModel);
    }

    //Add Medication to Drone
    @PatchMapping("/addmedication/{serialNumber}")
    public String AddMedication(@RequestBody Medication medication, @PathVariable String serialNumber){
        return _droneService.AddMedication(medication,serialNumber) ;
    }

    //Get Drone Items using Drone Serial Number
    @GetMapping("/getDroneItems/{serialNumber}")
    public List<Medication> getItems(@PathVariable String serialNumber){
        return _droneService.getDroneItems(serialNumber);
    }

    //Get available Drones on IDLE state
    @GetMapping("/getAvailableDrones")
    public List<Drone> getAvailableDrones(){
        return _droneService.getAvailableDrones();
    }

    //Check Drone Battery by serial number
    @GetMapping("/droneBattery/{serialNumber}")
    public String getBatteryLevel(@PathVariable String serialNumber){
        return _droneService.getBatteryLevel(serialNumber);
    }

    //Set drone State
    @PatchMapping("/setDroneState")
    public String setDroneState(@RequestBody DroneStateDao droneStateDao){
        return _droneService.setDroneState(droneStateDao.serialNumber, droneStateDao.state);
    }
}
