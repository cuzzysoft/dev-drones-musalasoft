package musala.dev.DroneAPI.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import musala.dev.DroneAPI.enums.DroneModel;
import musala.dev.DroneAPI.enums.DroneState;

import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="Drone")
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @JsonProperty
    private String serialNumber;
    @JsonProperty
    private DroneModel droneModel;
    @JsonProperty
    private int weightLimit;
    @JsonProperty
    private int batteryCapacity;
    @JsonProperty
    @Enumerated(EnumType.STRING)
    private DroneState droneState;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name="drone_id")
    private List<Medication> medications = new ArrayList<>();

    public Drone(){

    }
    public Drone(String serialNumber, DroneModel droneModel, int weightLimit, int batteryCapacity, DroneState droneState, List<Medication> medications){
        this.serialNumber=serialNumber;
        this.droneModel = droneModel;
        this.weightLimit=weightLimit;
        this.batteryCapacity=batteryCapacity;
        this.droneState = droneState;
        this.medications=medications;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public DroneModel getModel() {
        return droneModel;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public DroneState getState() {
        return droneState;
    }

    public List<Medication> getMedications() {
        return medications;
    }

    public void setMedications(List<Medication> medications) {
        this.medications = medications;
    }

    public void setDroneState(DroneState droneState) {
        this.droneState = droneState;
    }
}
