package musala.dev.DroneAPI.repository;

import musala.dev.DroneAPI.enums.DroneModel;
import musala.dev.DroneAPI.enums.DroneState;
import musala.dev.DroneAPI.model.Drone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneRepository extends CrudRepository<Drone, Long> {
    Iterable<Drone> findByDroneModel(DroneModel droneModel);
    List<Drone> findByDroneState(DroneState droneState);
    Drone findBySerialNumber(String serialNumber);
}
