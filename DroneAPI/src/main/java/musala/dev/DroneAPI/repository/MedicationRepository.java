package musala.dev.DroneAPI.repository;

import musala.dev.DroneAPI.model.Medication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {

}
