package musala.dev.DroneAPI.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Medication")
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    private int weight;
    @Column
    private String code;
    @Column
    private String image;


    public Medication(){

    }
    public Medication(String name, int weight, String code, String image){
        this.name=name;
        this.weight=weight;
        this.code=code;
        this.image=image;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getCode() {
        return code;
    }

    public String getImage() {
        return image;
    }
}
