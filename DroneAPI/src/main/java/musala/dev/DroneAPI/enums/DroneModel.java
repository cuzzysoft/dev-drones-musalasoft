package musala.dev.DroneAPI.enums;

public enum DroneModel {
    LightWeight,
    MiddleWeight,
    CruiseWeight,
    HeavyWeight
}
