# DEV DRONES MUSALASOFT

## Getting started
 
This Drone Service API is a Spring Boot Maven application written in Java 17 and it is advised to use same version for running.

The software models a Drone API for delivering medications to areas not easily accessible by land.

i. The Drone has six states (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).

ii. A drone is considered active when it is in DELIVERING, DELIVERED, RETURNING states. Then the battery status of the drone is 
    logged to a file called 'Battery log.txt' every 5 seconds. The battery level is also reduced by one for each run.


## Build and run
- Clone this repo to your desired location.

- Intellij IDE was used for the development but you can use your preferred IDE

- Import the cloned file to the IDE and click run.


## Tools

- H2 in-memory database was used for data storage
- Spring web
- Spring Data JPA

